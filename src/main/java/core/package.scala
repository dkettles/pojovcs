/** Shared type definitions
  */
package object core {

  /** Shorthand for Function1[A,A].  It represents diffs (operations).
    */
  type Endomorphism[A] = A => A

  /** A diff representing no change between this node and its parent
    */
  def NoOp[A](a: A) = {a}

}
