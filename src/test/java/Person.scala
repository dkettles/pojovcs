import core.Endomorphism

/** A class used for tests. */
class Person(val name: String)

object PersonOperations {

  def changeName(newName: String): Endomorphism[Person] = {p: Person => new Person(newName)}

  def flakyOperation(): Endomorphism[Person] = {
    if(true){throw new RuntimeException}
    {a: Person => a} // Just to satisfy the compiler
  }

}
