POJOVCS: Version Control for Your JVM Objects
=============================================

# What It Is

Just like it sounds: a version control system for JVM objects.  It let's you maintain a history of your object
using standard version control operations such as commit, branch, and merge.

# How Do I Use It?

The interface is designed to be similar your standard version control systems's interface.  For example, suppose 
you had an `Employee` class along with operations on instances:

```
#!scala
class Employee(val name: String, val email: String)

object EmployeeOperations {
  def changeName(newName: String): Endomorphism[Employee] = {employee: Employee => new Employee(newName, employee.email)}

  def changeEmail(newEmail: String): Endomorphism[Employee] = {employee: Employee => new Employee(employee.name, newEmail)}
  }

```

Then you can create an object history (known as a `Project`) and commit to it like this:

```
#!scala
val originalEmployee = new Employee("David Kettlestrings", "david.kettlestrings@company.com")
val project = new Project[Employee](originalEmployee, "employee-history", BasicIDGenerator)
val addMiddleInitial = EmployeeOperations.changeName("David E. Kettlestrings")
project.commit(Seq(addMiddleInitial), project.getBranch("master"))

// Latest has changes
project.getBranch("master").latest.resolve().name should be ("David E. Kettlestrings")
project.getBranch("master").latest.resolve().email should be ("david.kettlestrings@company.com")

//The one before (the original) is unchanged
project.getBranch("master").beforeLatest(1).resolve().name should be ("David Kettlestrings")
project.getBranch("master").beforeLatest(1).resolve().email should be ("david.kettlestrings@company.com")

```

Instead of putting a full set of examples here, they are placed within the `examples` package inside of `test` so that 
they are ensured to be up-to-date.  Also, all these examples (including this test) have a bunch more exposition 
explaining what's going on.  Think of them as tutorials.

# Concepts

## Definitions

* **Project** - This maps to the concept of a project in the version control repository sense.  It contains all of 
the history (branches, commits, etc.) for a *single object*.

* **Node** - A single node in the Project's history graph.  Nodes corresponds to individual commits, branch events, 
and merge events.

* **Branch** - An ordered, linear sequence of Nodes sharing a name (the branch name).

* **Endomorphism** - A function `A => A` where A is the type (class) of the object in the Project.  These are the
diffs between successive states in the object's history.

## Design

The most fundamental aspect of the design of this project is that **diffs**, not state, are the fundamental
mechanism in storing an object's history.  That is, instead of storing a new state and having the version control
system "figure out" the diff between them, it is up to the client to specify the diff and the version control 
mechanism will "figure out" (resolve) the state.

# How Do I Build/Test It?

It's a standard Maven build.  Go to the root of the project and execute

`mvn clean install`

which will execute all of the tests as well.

# Contributing

If you want to contribute, that would be great.  However, you are encouraged to first 
open a ticket to make sure that your enhancement isn't already planned/being worked on.

When contributing, please submit a pull request by following the practice below:

1. `git branch <branchname>`
2. `git checkout <branchname>`
3. Make changes and commit using a message of the form `issue #4 Some message`
4. `git push --set-upstream origin <branchname>`
5. Log into Bitbucket and create a pull request

# License

MIT License

Copyright (c) [2016] [David Kettlestrings]

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.